__author__='Jorge Runza'
from unittest import TestCase
from Calculadora import *

class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEquals(Calculadora().sumar(""), 0, "Cadena vacia")

    def test_sumar_unacadena(self):
        self.assertEquals(Calculadora().sumar("1"), 1, "Un Numero")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEquals(Calculadora().sumar("1"), 1, "Un Numero")
        self.assertEquals(Calculadora().sumar("2"), 2, "Un Numero")

    def test_sumar_cadenaConDosNumeros(self):
        self.assertEquals(Calculadora().sumar("1,3"), 4, "Dos Numeros")

    def test_sumar_cadenaConMultiplesNumeros(self):
        self.assertEquals(Calculadora().sumar("5,2,4,1"), 12, "Multiples Numeros")

    def test_sumar_cadenaConMultiplesNumerosConSeparadores(self):
        self.assertEquals(Calculadora().sumar("5,2&4:1:2&8"), 22, "Multiples Numeros distintos separadores")